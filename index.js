const Discord = require('discord.js');
const { prefix, token } = require('./config.json');
const client = new Discord.Client();

client.once('ready', () => {
	console.log('boner!');
});
client.once
//base
client.on('message', message => {
	if (!message.content.startsWith(prefix) || message.author.bot) return;

	const args = message.content.slice(prefix.length).split(/ +/);
	const command = args.shift().toLowerCase();

	if (command === 'bruh') {
		message.channel.send('moment');
	}  else if (command === 'ping') {
		message.channel.send('Pong.');
	} else if (command === 'beep') {
		message.channel.send('Boop.');
	} else if (command === 'server') {
		message.channel.send(`Server name: ${message.guild.name}\nTotal members: ${message.guild.memberCount}`);
	} else if (command === 'user-info') {
		message.channel.send(`Your username: ${message.author.username}\nYour ID: ${message.author.id}`);
	} else if (command === 'hello') {
		message.channel.send('Please, leave me alone until after the experiment.');
	} else if (command === 'walterquestions') {
		message.channel.send('I died in Half-Life 1.');
	} else if (command === 'retro') {
		message.channel.send('unsung hero');
	} else if (command === 'bed') {
		message.channel.send('You may not sleep now, monsters are nearby');
	} else if (command === 'Whoisthecreator') {
		message.channel.send('@Retro#5353');
	} else if (command === 'play') {
		message.channel.send('Little do they know, Hard hat Jones doesnt take requests.');	
	} else if (command === 'help') {
		message.channel.send('Try the following commands, walterquestions, Whoisthecreator, hello, server.');
	} else if (command === 'info') {
		if (!args.length) {
			return message.channel.send(`You didn't provide any arguments, ${message.author}!`);
		} else if (args[0] === 'foo') {
			return message.channel.send('bar');
		}
	} else if (command === 'profilepicture') {
		if (!message.mentions.users.size) {
			return message.channel.send(`Your Profile Picture: <${message.author.displayAvatarURL}>`);
		}

		const avatarList = message.mentions.users.map(user => {
			return `${user.username}'s avatar: <${user.displayAvatarURL}>`;
		});

		message.channel.send(avatarList);
	} else if (command === 'prune') {
		const amount = parseInt(args[0]) + 1;

		if (isNaN(amount)) {
			return message.reply('that doesn\'t seem to be a valid number.');
		} else if (amount <= 1 || amount > 100) {
			return message.reply('you need to input a number between 1 and 99.');
		}

		message.channel.bulkDelete(amount, true).catch(err => {
			console.error(err);
			message.channel.send('there was an error trying to prune messages in this channel!');
		});
	}
});


//client id

client.login('no');
